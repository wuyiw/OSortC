#OSortC
用于对osc首页各版块自定义排序.

## 安装说明 (via: [八宝粥](http://git.oschina.net/shentu-jiazhen/Block-eechen))

#### 直接下载crx安装

 1. 点[附件](http://git.oschina.net/wuyiw/OSortC/attach_files)下载最新的crx
 2. 拖拽crx文件到chrome扩展程序页面

#### 打包安装

 1. 点[Zip](http://git.oschina.net/wuyiw/OSortC/repository/archive/master)下载最新源码
 2. 解压源码
 3. 在chrome扩展程序页面勾选开发者模式
 4. 点击‘加载已解压的扩展程序’ 选择源码解压目录

## 使用
安装后打开[osc首页](http://www.oschina.net), 鼠标移到顶部导航栏"城市圈"右边, 有个默认隐藏的"排序"链接, 点进去即可拖动版块进行排序, 不过只能上下挪-.-, 完了之后点顶部的"保存"即可. ("保存"右边是"重置", 如果osc改版或其他原因让版面错乱, 点它即可)




不过...这东西真的有卵用吗...