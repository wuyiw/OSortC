﻿;(function() {
		var container = $('#OSC_Content > table > tbody > tr > td')
			, toSorts = container.children('table, div[id], .RecommBlogs') // 要进行排序的板块(资讯, 众包, 动弹...)
			, isSortingPage = location.search == '?sort'
		chrome.storage.sync.get('orders', function(items) {
			var orders = items.orders
			if (Array.isArray(orders)) { // 若有已保存排序, 排排排
				if (orders.length != toSorts.length) { // osc版面有变更时可能错乱
					console.warn('OSortC: 排序记录和页面不一致, 若页面错乱请重排一次.')
				}
				$('<span>').insertBefore(toSorts).after(function(i) {
					return toSorts.eq(orders[i])
				}).remove()
			}
			
			if (isSortingPage) { // 当前为排序页
				container.children().not(toSorts).hide() // 隐藏不参与排序的元素
				toSorts.attr('sort_order', function(i) { return i; }) // 标记初始位置
				container.sortable() // jquery-ui sortable
				$('#OSC_Channels ul').append($('<li>', {
					class: 'item'
				}).append($('<a>', {
					text: '保存',
					href: '#'
				}).click(function() { // 保存排序并返回首页
					chrome.storage.sync.set({
						orders: $('[sort_order]').map(function() {
							return this.getAttribute('sort_order')
						}).get()
					}, function() {
						location.href = '/'
					})
				})), $('<li>', {
					class: 'item'
				}).append($('<a>', {
					text: '重置',
					href: '#'
				}).click(function() { // 重置排序并刷新
					if (confirm('确定要重置吗?')) {
						chrome.storage.sync.remove('orders', function() {
							location.reload()
						})
					}
				})))
			} else {
				$('<li>', {
					class: 'item hover_to_display'
				}).append($('<a>', { // 进入排序页面的链接
					text: '排序',
					href: '/?sort'
				})).appendTo($('#OSC_Channels ul'))
			}
		})
})()